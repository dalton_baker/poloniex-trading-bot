/**************************************************************************//**
 * @file
 * @brief This contains all the members of the ChartData class
 *****************************************************************************/
#include "chart.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the constructor for the ChartData class. You will need to provide an
 * an input stream as a parameter. The data needs to be provided in a very
 * specific order:
 * 1) The currency pair you are dealing with. Not completely necessary.
 * 2) The current balance of currencty 1. This currency is what the other
 *    currencies value is defined in terms of. So, it is always just considered
 *    to be worth 1 of itself.
 * 3) The current balance of currency 2.
 * 4) The high bid for currency 2 in terms of currency 1.
 * 5) The low ask for currency 2 in term of currency 1.
 * 6) Finally, the chart data. You can enter any number of values you would
 *    like, assuming your computer has the space to hold all of them. However,
 *    you cannot get any averages for a longer period than data points you have
 *    provided. So if you only enter 10 values, calling "SMA(20)" will force
 *    the whole program to crash. BE CAREFULL!!!
 *
 * @param[in] fin - an input variable
 *
 *****************************************************************************/
ChartData::ChartData(istream &fin)
{
   double reader;

   //get the name of the pair
   fin >> pair;

   //read in the ballances of our 2 currencies
   fin >> curr1bal;
   fin >> curr2bal;

   //get the current bids and asks for the market
   fin >> highBid;
   fin >> lowAsk;

   //read in all the chart data
   while(!fin.eof())
   {
      fin >> reader;
      chart.push_front(reader);
   }
   //pop the last data read in, for some reason it grabs the last one twice
   chart.pop_front();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return a simple moving average for a specified period.
 *
 * @param[in] period - The period of time you need an average from
 *
 * @return - the simple moving average of the chart data for the provided period
 *
 *****************************************************************************/
double ChartData::SMA(int period)
{
   //get some temporary variables to hold our values
   double average = 0.0;
   deque<double> tempChart = chart;

   //add the last perriod number of items together
   for( int i = 0; i < period; i++)
   {
      average += tempChart.front();
      tempChart.pop_front();
   }

   //return the added values devided by our period
   return average / double(period);
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This wil give you an exponential moving average from the provided chart data
 * for the period of time provided. At first glance this doesn't look like a
 * normal eponential moving average equation, but it certainly is. I reworked
 * the original equation and turned it into something easier to program.
 *
 * @param[in] period - The period of time you need an average from
 *
 * @return - the exponential moving average
 *
 *****************************************************************************/
double ChartData::EMA(int period)
{
   //get some temporary variables
   double smoothFact = 2.0/((double)period+1);
   double accumulator = 0.0;
   double exp = 1.0;
   deque<double> tempChart = chart;

   //add up all the calculatable values for the EMA's
   for(int i = 0; i < period; i++)
   {
      accumulator += chart.front() * exp;
      chart.pop_front();
      exp *= (1 - smoothFact);
   }

   //multiply all the calculated EMA's by the smoothing factor
   accumulator *= smoothFact;

   //get the initial value, must use SMA since we can't get EMA to infinity
   accumulator += (SMA(period)*exp);

   //reset the original chart values
   chart = tempChart;

   //return the result
   return accumulator;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the balance of currency 1 in terms of currency 2 according
 * to the current lowest asking price (the lowest price you can buy it for) for
 * currency 2.
 *
 * @return - the balance
 *
 *****************************************************************************/
double ChartData::convBal1to2()
{
   return curr1bal / lowAsk;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the balance of currency 2 in terms of currency 1 according
 * to the highest bid price (the highest price you can sell it for) for
 * currency 2.
 *
 * @return - the return information
 *
 *****************************************************************************/
double ChartData::convBal2to1()
{
   return curr2bal * highBid;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will just return an array containing the 2 names of your currencies
 *
 * @return - the currency pair
 *
 *****************************************************************************/
char* ChartData::getPair()
{
   return pair;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the current balance of currency 1
 *
 * @return - the balance of currency 1
 *
 *****************************************************************************/
double ChartData::getBal1()
{
   return curr1bal;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the current balance of currency 2
 *
 * @return - the balance of currency 2
 *
 *****************************************************************************/
double ChartData::getBal2()
{
   return curr2bal;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the highest bid for currency 2 in terms of currency 1
 *
 * @return - the high bid
 *
 *****************************************************************************/
double ChartData::getBid()
{
   return highBid;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the low ask for currency 2 in terms of currency 1
 *
 * @return - the rlow ask
 *
 *****************************************************************************/
double ChartData::getAsk()
{
   return lowAsk;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return all of the chart data as a deque
 *
 * @return - the chart data
 *
 *****************************************************************************/
deque<double> ChartData::getChart()
{
   return chart;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the chart size, or how many data points you privided earlier
 *
 * @return - the chart size
 *
 *****************************************************************************/
int ChartData::getChartSize()
{
   return chart.size();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will print a specified number of data points from the chart to
 * the screen.
 *
 *****************************************************************************/
void ChartData::printChart(int numOfPoints)
{
   int count = 1;

   for( auto x : chart )
   {
      cout << count << ": "<< x << endl;
      count++;

      if(count > numOfPoints)
      {
         return;
      }
   }
}
