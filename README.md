This project was created to be a trading bot for crypto currencies on the
trading platform poloniex.com. I chose poloniex because it had a very nice, easy
to use API. After some extensive testing I had to basically abandon this
altogether because I discovered that the chart data returned by poloniex's API
was wrong a significant portion of the time. This lead to bad decision making
by my program and ultimately would leak money constantly. The goal of this was
not to make money necessarily, but to increase the number of coins of one type
that was in your portfolio.

Originally the plan was to monitor a 50 point simple moving average against a
200 point simple moving average. If the 50 MA went above the 200 MA then it
could be assumed that the price was suddenly going up and it was time to buy.
In the same way if the 50 MA was below the 200 MA it was time to sell. This idea
only really worked on paper and became much more complicated as time went on. I
eventually realized 50 and 200 probably weren't the best combination of
averages to use. from here on out I will just call them large and small values.

I tested a significant number of different combinations of moving averages. I
used the poloniex API to download a little over a years worth of data and would
run 8 simulations simultaneously repeatedly with different settings. I
eventually got smart enough to have the tests print the results off as files,
instead of just printing the results to the screen. There are several of these
test files in the "debug" folder. I believe the best results I ever got was with
small of 40 and a large of 100. In any case, as I stated before, all the data I
got from poloniex was wrong anyway so it doesn't really matter. Each time I ran
a simulation, it would take hours to complete. I spent several weeks just
running simulations to try and find the best large and small combination to use.

At some point I realized an exponential moving average would work much better
for detecting up and down trends faster. They react to the market much quicker.
So I added a exponential moving average to my ChartData class in the data
processing folder. by using a small EMA and a large MA I was able to get
significantly better results. This was the point that I realized poloniex's
API was flawed.

I abandoned poloniex and started working on a different platform. All of the
data for that is in a different folder and is quite unorganized. I worked on it
for quite a while but eventually abandoned it as well. I may return to it one
day, but it's a mess for the time being.

One of the major issues I ran into later was that poloniex was the only place I
could download the type of data I was looking for. The crypto currency markets
are extremely volatile, so I want to be able to pull a new chart value for every
5 minute increment. additionally for testing I want to get a few months of data
at least, if not significantly more. poloniex is the only service I found that
offered that, but their data was extremely unreliable.

The next major issue I ran into was that I failed to take into account trading
fees. every transaction you do takes 0.3% of the traded crypto as a fee. Well,
when I ran my program with no fees it made a significant amount, but with the
fees included it would loose money. Over the course of a year, it would end up
completely broke.

One way I tried to counter act this was to include some code that would reject a
trade if the trade was going to result in less crypto than it had the last time
a trade was done. This worked ok for a while, but would eventually stall out and
stop trading altogether. After closer inspection I realized that the price of
one of the currencies would rise to a new "normal" value. For instance a ripple
coin was originally worth around 0.00002 bitcoin. After several months it was
hovering around 0.0004 bitcoin. So, if the program sold ripple at 0.00002
bitcoin each, it would refuse to ever buy it again. This was extremely
frustrating, and eventually I got far too busy with other stuff to put much
time into solving this issue.

I'm sure there is a way to make this work properly. However, I am a bit to busy
to do much more with it. I may pick it back up someday, but I will take a much
different approach. This was mostly just a fun project to do while I was on
summer break from school. I figured I might as well upload some of the code I
wrote, just so someone might see it and find it useful someday. This project
became significantly more complex than I originally anticipated. I was naive and
though it would be relatively simple.

For this project I used a combination of shell scripts, python, and c++. I
wanted to use c++ to analyze the data, I just think it's better and easier to
program math equations in. I hadn't used python before this project and thought
it was the best option for manipulating files and reorganizing data because of
the way variables and lists work for it. I used shell scripts to tie everything
together and retrieve data from the internet. To run the program, open a
terminal and type:
`sh poloniexScript.sh large small period_in_seconds currency_1 currency_2`
So it might look something like this:
`sh poloniexScript.sh 200 50 300 BTC XRP`
In that example 200 is the large, 50 is the small, the period is 5 minutes, and
we are looking at the BTC_XRP currency pair.

I used a series of files to transfer info from each part of the program. So, 
here's how it works. First the main script gets called, it calls three other
scripts first. These scripts get your balances, past chart data, and the current 
ticker data. It shoves that info into 3 different files. Then it calls a python 
program, which takes those three files and converts the data into the exact form 
it needs to be in for the c++ data processing program to read it, and shoves it
into another file. It then feeds the contents of that file into the data
processing program as input. If the data processing program sends out a trade
order, it will be in the exact form to serve as the arguments for the trade
order script. It will then add the trade to a history file and call the trade 
script to execute it.

If you want to change whether you are using simple moving averages or
exponential moving averages, you need to modify the c++ code directly.
Additionally, you will need to put your API Key and Secret into the files 
"getBalances.sh" and "sendTradeOrder.sh" files in the "scripts" folder. You will
also need to add your computer to the list of trusted IP addresses on the
poloniex website.

If you want to do some testing you can use the debug folder. Inside there are a 
couple folders. One has past test data that I will leave in it. Another has a
tool that I made to get a bunch of old data at once. If I remember right, you 
can only pull about a months worth of data at a time. The you can use cat to 
string all the files together. Then inside of the "massTests" folder you will 
see several test folders. Each folder contains the same thing, I only made so 
many so I could run several at the same time. The scripts inside these folders 
will pull data from the "chartMaster.dat" file located in the "getMassData" 
folder a few directories above it. Additionally, all of the test folders will 
use the same c++ data processing program in the main dirrectory. That way you
only need to compile it once for all of them. 
