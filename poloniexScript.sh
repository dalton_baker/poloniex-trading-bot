#This is the script that will repeatedly run the poloneix programs

#if there aren't enough arguments break out!!
if [ $# -ne 5 ]; then
   echo "Incorrect number of arguments for main script!!!"
   exit 1
fi

#clear the data file
> ./dataFiles/history.dat

#initialize all the variables
LARGE=$1
SMALL=$2
PERIOD=$3
CURR1=$4
CURR2=$5

while true
do
   > ./dataFiles/ticker.dat
   echo
   echo "======================== " $(date) " ========================"
   #call the script to get the ticker data
   sh ./scripts/getChartData.sh $CURR1"_"$CURR2 $(($LARGE * 2)) $PERIOD > ./dataFiles/chart.dat
   #call the script to get your balances
   sh ./scripts/getBalances.sh > ./dataFiles/balances.dat
   #call the script to get the current ticker
   sh ./scripts/getCurrentTicker.sh > ./dataFiles/currTicker.dat

   #Call the python program to get the data from poloneix
   python ./pythonFiles/convertDataFiles.py $CURR1 $CURR2

   #Call the C++ program to process the data
   result=`./cppFiles/dataProcessing $LARGE $SMALL < ./dataFiles/ticker.dat`

   #if the program output something, then we need to execute the trade
   if [ ! -z "$result" ]; then
      echo $(date) " : " $result >> ./dataFiles/history.dat
      sh ./scripts/sendTradeOrder.sh $result
      echo
   fi

   #Wait for a while before you pull the data again
   sleep $PERIOD
done
