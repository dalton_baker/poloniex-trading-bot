#if there aren't enough arguments break out!!
if [ $# -ne 3 ]; then
   echo "Incorrect number of arguments for chart retreival!!!"
   exit 1
fi

#Set all the variables to their arguments
PAIR=$1
NUMPOINTS=$2
PERIOD=$3

#Get the current time
CURRENTEPOCH=`date "+%s"`
#get the start time
STARTEPOCH=$(($CURRENTEPOCH - (($PERIOD * $NUMPOINTS))))

#output data in json format
echo -n "{\"chart\":"
curl "https://poloniex.com/public?command=returnChartData&currencyPair="$PAIR"&start="$STARTEPOCH"&end="$CURRENTEPOCH"&period="$PERIOD
echo -n "}"
