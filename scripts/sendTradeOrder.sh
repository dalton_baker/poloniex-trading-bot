#This is my secret api data, Don't let anyone get it!!!!
API_KEY="your_key_here"
API_SECRET="your_secret_here"
NONCE=`date "+%s00000"`

#if there aren't enough arguments break out!!
if [ $# -ne 4 ]; then
   echo "Incorrect number of arguments for trade execution!!!"
   exit 1
fi

#Get the data that was passed in
#pair will be BTC_XRP most of the time
PAIR=$1
#command will be 'buy' or 'sell'
COMMAND=$2
#conversion rate
RATE=$3
#number of coins to buy or sell
AMOUNT=$4

#this will generate the sign and then make it the correct format
API_SIGN=$(echo -n "command="$COMMAND"&currencyPair="$PAIR"&rate="$RATE"&amount="$AMOUNT"&nonce="$NONCE"&immediateOrCancel=1" | \
openssl sha512 -hmac $API_SECRET)
API_SIGN=${API_SIGN#"(stdin)= "}

#put in th eactual order
curl -X POST \
   -d "command="$COMMAND"&currencyPair="$PAIR"&rate="$RATE"&amount="$AMOUNT"&nonce="$NONCE"&immediateOrCancel=1" \
   -H "Key: "$API_KEY \
   -H "Sign: "$API_SIGN \
   https://poloniex.com/tradingApi
